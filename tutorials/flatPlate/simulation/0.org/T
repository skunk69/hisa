/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volScalarField;
    location    "0";
    object      T;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include        "include/freestreamConditions"

dimensions      [0 0 0 1 0 0 0];

internalField   uniform $T;

boundaryField
{
    inlet
    {
        type            characteristicFarfieldTemperature;
        #include        "include/freestreamConditions"
    }
    freestream
    {
        type            characteristicFarfieldTemperature;
        #include        "include/freestreamConditions"
    }
    inviscidWall
    {
        type            zeroGradient;
    }
    viscousWall
    {
        type            characteristicWallTemperature;
        value           uniform 0;
    }
    outlet
    {
        type            zeroGradient;
    }
}


// ************************************************************************* //
